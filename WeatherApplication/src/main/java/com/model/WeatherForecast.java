package com.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherForecast implements Serializable{
	private static final long serialVersionUID = -6330775871474052703L;

	private int count;
	
	private List<WeatherInfo> weatherInfoList;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<WeatherInfo> getWeatherInfoList() {
		return weatherInfoList;
	}

	public void setWeatherInfoList(List<WeatherInfo> weatherInfoList) {
		this.weatherInfoList = weatherInfoList;
	}
}
