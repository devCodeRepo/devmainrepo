package com.client;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import com.model.WeatherForecast;
import com.model.WeatherInfo;

@Component
public class WeatherServiceClient implements CommandLineRunner {

    private static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/forecast?zip={zip}&appid=96c9189b4377490de31ea173a120bbc5";

    private void callWeatherService(String zip) {

        RestTemplate restTemplate = new RestTemplate();
        URI url = new UriTemplate(WEATHER_URL).expand(zip);
        WeatherForecast weatherForecast = restTemplate.getForObject(url, WeatherForecast.class);

        if (weatherForecast != null && weatherForecast.getWeatherInfoList()!=null && !weatherForecast.getWeatherInfoList().isEmpty()) {
            printWeatherInfo(weatherForecast.getWeatherInfoList());
        } else {
            System.out.println("Weather info unavailable");
        }
    }

    private void printWeatherInfo(List<WeatherInfo> weatherInfoList) {
        System.out.println("WeatherForecast Count= " + weatherInfoList.size());

        LocalDateTime tomorrow = LocalDateTime.now(ZoneId.of("America/New_York")).plus(1, ChronoUnit.DAYS);

        for (WeatherInfo weatherEntry : weatherInfoList) {

            LocalDateTime entryDateTime = LocalDateTime.ofInstant(weatherEntry.getTimestamp(), ZoneOffset.UTC);

            if (entryDateTime.getDayOfMonth() == tomorrow.getDayOfMonth()) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

                String formattedDate = entryDateTime.format(formatter);

                System.out.println("Predicted temperature for tomorrow : " + formattedDate + " : "
                        + weatherEntry.getCelsiusTemperature() + " Degree Centrigate");
            }
        }
    }

    @Override
    public void run(String... args) {

        boolean enter = true;
        try (Scanner scanner = new Scanner(System.in)) {
            while (enter) {
                System.out.print("Enter zip code ");

                String zip = scanner.next();
                if (!"exit".equalsIgnoreCase(zip)) {
                    String regex = "^[0-9]{5}(?:-[0-9]{4})?$";
                    Pattern pattern = Pattern.compile(regex);
                    Matcher matcher = pattern.matcher(zip);
                    if (matcher.matches()) {
                        callWeatherService(zip);
                    } else {
                        System.out.println("Please enter a valid zip code for USA.");
                    }

                } else {
                    enter = false;
                }

            }
        } catch (Exception e) {
            System.out.println("Unable to process at this time,Please try again later. Zip code not Available");
            e.getMessage();
        }

    }
}
